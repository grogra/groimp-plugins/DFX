
package de.grogra.ext.dxf;

import java.io.IOException;
import java.io.PrintWriter;

import javax.vecmath.Matrix4d;
import javax.vecmath.Tuple4d;

import de.grogra.imp3d.PolygonArray;
import de.grogra.imp3d.io.SceneGraphExport;
import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.imp3d.objects.SceneTree.Leaf;

public abstract class ObjectBase implements SceneGraphExport.NodeExport
{
	// getGraphState
	
	Matrix4d transformation;
	PrintWriter out;

	public void export (Leaf node, InnerNode transform, SceneGraphExport sge)
			throws IOException
	{
		// TODO Auto-generated method stub
//		System.err.println ("export was called");

		// convert to DXFExport
		DXFExport export = (DXFExport) sge;

		// obtain output writer
		out = export.out;

		// obtain transformation matrix for this node
		Matrix4d m = (Matrix4d) export.matrixStack.peek ();
		Matrix4d n = new Matrix4d ();
		if (transform != null)
		{
			transform.transform (m, n);
		}
		else
		{
			n.set (m);
		}
		transformation = n;

		// call implementation export function
		exportImpl (node, transform, export);
	}

	abstract void exportImpl (Leaf node, InnerNode transform, DXFExport export)
			throws IOException;

	Matrix4d getTransformation ()
	{
		return transformation;
	}

	// round to 5 decimal digits
	static double round (double d)
	{
		return (double) Math.round (d * 100000) / 100000;
	}

	static void triangle (PrintWriter out, double x0, double y0, double z0,
			double x1, double y1, double z1, double x2, double y2, double z2)
	{
		// entity type
		out.println ("0");
		out.println ("3DFACE");
		// first vertex
		out.println ("10");
		out.println (round (x0));
		out.println ("20");
		out.println (round (y0));
		out.println ("30");
		out.println (round (z0));
		// second vertex
		out.println ("11");
		out.println (x1);
		out.println ("21");
		out.println (y1);
		out.println ("31");
		out.println (z1);
		// third vertex
		out.println ("12");
		out.println (x2);
		out.println ("22");
		out.println (y2);
		out.println ("32");
		out.println (z2);
	}

	static void quad (PrintWriter out, double x0, double y0, double z0,
			double x1, double y1, double z1, double x2, double y2, double z2,
			double x3, double y3, double z3)
	{
		// entity type
		out.println ("0");
		out.println ("3DFACE");
		// first vertex
		out.println ("10");
		out.println (round (x0));
		out.println ("20");
		out.println (round (y0));
		out.println ("30");
		out.println (round (z0));
		// second vertex
		out.println ("11");
		out.println (round (x1));
		out.println ("21");
		out.println (round (y1));
		out.println ("31");
		out.println (round (z1));
		// third vertex
		out.println ("12");
		out.println (round (x2));
		out.println ("22");
		out.println (round (y2));
		out.println ("32");
		out.println (round (z2));
		// fourth vertex
		out.println ("13");
		out.println (round (x3));
		out.println ("23");
		out.println (round (y3));
		out.println ("33");
		out.println (round (z3));
	}

	static void quad (PrintWriter out, Tuple4d p0, Tuple4d p1, Tuple4d p2,
			Tuple4d p3)
	{
		quad (out, p0.x, p0.y, p0.z, p1.x, p1.y, p1.z, p2.x, p2.y, p2.z, p3.x,
			p3.y, p3.z);
	}

	void beginPolyline (int vertices, int faces)
	{
		// generate a polyline
		out.println ("0");
		out.println ("POLYLINE");
		// polyline flag: 64 = polyface mesh
		out.println ("70");
		out.println ("64");
		// number of vertices
		out.println ("71");
		out.println (vertices);
		// number of faces
		out.println ("72");
		out.println (faces);
	}

	void endPolyline ()
	{
		out.println ("0");
		out.println ("SEQEND");
	}

	void mesh (PolygonArray p)
	{
		if (p.polygons.size == 0)
		{
			return;
		}
		// begin mesh
		beginPolyline (p.vertices.size () / p.dimension, p.polygons.size ()
			/ p.edgeCount);

		// output vertices
		for (int i = 0; i < p.vertices.size (); i += p.dimension)
		{
			out.println ("0");
			out.println ("VERTEX");
			out.println ("10");
			out.println (round (p.vertices.get (i)));
			out.println ("20");
			out.println (round (p.vertices.get (i + 1)));
			out.println ("30");
			out.println (round (p.vertices.get (i + 2)));
			out.println ("70"); // vertex flags
			out.println ("192");
		}

		// output indices
		for (int i = 0; i < p.polygons.size (); i += p.edgeCount)
		{
			out.println ("0");
			out.println ("VERTEX");
			out.println ("10");
			out.println ("0");
			out.println ("20");
			out.println ("0");
			out.println ("30");
			out.println ("0");
			out.println ("70"); // vertex flags: 128 = index
			out.println ("128");
			for (int j = 0; j < p.edgeCount; j++)
			{
				out.println ("7" + (j + 1));
				out.println (p.polygons.get (i + j) + 1);
			}
		}

		// end mesh
		endPolyline ();
	}

}
